import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';

export default {
    constructValidPartnerRepOAuth2AccessToken
}

function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.userId,
        "account_id": accountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}