import SpiffApiGatewayServiceSdk from '../../src/index';
import SpiffEntitlementWithPartnerRepInfoWebDto from  '../../src/spiffEntitlementWithPartnerRepInfoWebDto';
import UploadPartnerSaleInvoiceReqWebDto from '../../src/uploadPartnerSaleInvoiceReqWebDto';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be SpiffApiGatewayServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(SpiffApiGatewayServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listPartnerRepsInfoWithAccountId method', () => {
            it('should get the partner Reps Information, return type is List of partnerRepInfoWebView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig);

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .listPartnerRepsInfoWithAccountId(
                            dummy.accountId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        ).then(partnerRepInfoWebViews => {
                            return partnerRepInfoWebViews;
                        }
                    );

                /*
                 assert
                 */
                actPromise
                    .then(partnerRepInfoWebViews => {
                        expect(partnerRepInfoWebViews).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });

        describe('listSpiffEntitlementsAccountId method', () => {
            it('should return spiff entitlements, return type List of SpiffEntitlementWithPartnerRepInfoWebView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig);

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .listSpiffEntitlementsAccountId(
                            dummy.accountId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        ).then(SpiffEntitlementWithPartnerRepInfoWebView => {
                            return SpiffEntitlementWithPartnerRepInfoWebView;
                        }
                    );

                /*
                 assert
                 */
                actPromise
                    .then(SpiffEntitlementWithPartnerRepInfoWebView => {
                        expect(SpiffEntitlementWithPartnerRepInfoWebView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });

        describe('claimSpiffEntitlements method', () => {
            it('should post the spiff entitlements with partnerrep info, return type is List of claimSpiffWebView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig);

                let spiffEntitlementWithPartnerRepInfoWebDto = [];
                spiffEntitlementWithPartnerRepInfoWebDto.push(
                    new SpiffEntitlementWithPartnerRepInfoWebDto(
                        dummy.spiffEntitlementId,
                        dummy.partnerSaleRegistrationId,
                        dummy.installDate,
                        dummy.spiffAmount,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.invoiceNumber,
                        dummy.facilityName,
                        dummy.sellDate
                    )
                );

                /*
                 act
                 */
               const actPromise =
                    objectUnderTest
                        .claimSpiffEntitlements(
                            dummy.accountId,
                            spiffEntitlementWithPartnerRepInfoWebDto,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        ).then(claimSpiffWebViews => {
                                return claimSpiffWebViews;
                            }
                        );

                /*
                 assert
                 */
                actPromise
                    .then((claimSpiffWebViews) => {
                        expect(claimSpiffWebViews).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });

        /*describe('uploadInvoiceForPartnerSaleRegistration method', () => {
            it('should post the upload partnersale invoice, return type partnerSaleInvoiceWebView', (done) => {
                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig);


                let uploadPartnerSaleInvoiceReqWebDto = new UploadPartnerSaleInvoiceReqWebDto(
                                                                        dummy.partnerSaleRegistrationId,
                                                                        dummy.invoiceNumber,
                                                                        dummy.file
                                                                        );
                /!*
                 act
                 *!/
                const actPromise =
                    objectUnderTest
                        .uploadInvoiceForPartnerSaleRegistration(
                            uploadPartnerSaleInvoiceReqWebDto,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        ).then(partnerSaleInvoiceWebView => {
                            return partnerSaleInvoiceWebView;
                            }
                        );

                /!*
                 assert
                 *!/
                actPromise
                    .then(partnerSaleInvoiceWebView => {
                        console.log('partnerSaleInvoiceWebView',partnerSaleInvoiceWebView);
                        expect(partnerSaleInvoiceWebView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });*/
    });
});